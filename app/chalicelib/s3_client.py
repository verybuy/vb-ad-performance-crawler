import boto3
from botocore.exceptions import ClientError

class S3Client():
    """ This helper is a library for AWS S3 Service
        dependency on boto3

        Methods:
            upload: upload a file to an S3 bucket

        How to used:
            ```
            from {module_path} import S3Client

            s3_client = S3Client()
            s3_client.upload('source_path', 'bucket_name', 'remote_path')
            ```
    """
    __client = None

    def __get_client(self) -> boto3.client:
        if self.__client is None:
            self.__client = boto3.client('s3')
        return self.__client

    def upload(self, local_path: str, bucket: str, key: str) -> str:
        """ local_path: path of the file to upload
            bucket: name of the bucket
            key: name of the key to upload
        """
        client = self.__get_client()
        try:
            client.upload_file(local_path, bucket, key)
        except ClientError as e:
            raise e
        return self.generate_presigned_url(bucket, key)

    def read(self, bucket: str, key: str) -> str:
        client = self.__get_client()
        try:
            response = client.get_object(Bucket=bucket, Key=key)
            body = response['Body'].read()
        except ClientError as e:
            raise e
        return body

    def download(self: str, bucket: str, key: str, local_path: str) -> bool:
        client = self.__get_client()
        try:
            client.download_file(bucket, key, local_path)
        except ClientError as e:
            raise e
        return True

    def generate_presigned_url(self, bucket: str, key: str):
        client = self.__get_client()
        try:
            presigned_url = client.generate_presigned_url('get_object',
                                                          Params={
                                                              'Bucket': bucket,
                                                              'Key': key
                                                          },
                                                          ExpiresIn=1800)
        except ClientError as e:
            raise e
        return presigned_url
