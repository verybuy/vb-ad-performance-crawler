import re
from email.parser import Parser
from chalicelib.request import Request

class Mailer():

    _body = None
    _reference = None
    _mail_payloads = None
    _urls = None

    def __init__(self, body):
        self._body = Parser().parsestr(body.decode('utf-8'))

    def get_content(self):
        return self._body

    def get_reference(self):
        if self._reference is None:
            match = re.search('([\w+\.]*(?P<ref>facebook|google).com)', self._body.get('References', ''))
            if match:
                self._reference = match.group('ref')
            else:
                match = re.search('([\w+\.]*(?P<ref>facebook|google).com)', self._body.get('From', ''))
                if match:
                    self._reference = match.group('ref')

        return self._reference

    def fetch_mail_payloads(self):
        if self._mail_payloads is None:
            payloads = []
            for message in self.get_content().walk():
                encoded_type = message['Content-Transfer-Encoding']
                if encoded_type == 'base64' or encoded_type == 'quoted-printable':
                    payload = message.get_payload(decode=True)
                    if self.get_reference() != 'rtb':
                        payloads.append(payload.decode("utf-8"))
                    else:
                        payloads.append(payload)
            self._mail_payloads = payloads
        return self._mail_payloads

    def get_urls(self):
        if self._urls is None:
            urls = []
            regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
            for payload in self.fetch_mail_payloads():
                url = re.findall(regex, payload)
                urls = urls + [x[0] for x in url]
                self._urls = urls

        return self._urls

    def get_mail_download_link(self):
        urls = self.get_urls()
        if not urls:
            return ''

        cases = {
            "facebook": self._get_facebook_download_link,
            "google": self._get_google_download_link
        }
        func = cases.get(self.get_reference(), lambda x: '')
        return func(urls)

    def get_real_download_link(self, url=''):
        if not url:
            url = self.get_mail_download_link()

        if not url:
            return ''

        match = re.search('^http', url)
        if not match:
            url = "https://{}".format(url)

        req = Request.get_request(url)
        r = req.get(url, allow_redirects=False)
        # r.raise_for_status()
        if r.status_code == 302:
            url = r.next.url
            return self.get_real_download_link(url)

        return url


    def _get_facebook_download_link(self, urls):
        link = ''
        reg = '(?P<link>.*\.facebook.com\/ads\/report_builder\/past_report\/download_report/\?.*export_format=csv.*)'
        for url in urls:
            match = re.search(reg, url)
            if match:
                link = match.group('link')
                break
        return link

    def _get_google_download_link(self, urls):
        link = ''
        reg = '(?P<link>.*ads.google.com.*)'
        for url in urls:
            match = re.search(reg, url)
            if match:
                link = match.group('link')
                break
        return link
