import re, requests
from os import environ as env

class Request():
    def get_request(url):
        req = requests.Session()
        host = Request.get_authority_by_url(url)
        headers = {
            'authority': host,
            'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
            'sec-ch-ua-mobile': '?0',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site': 'none',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-user': '?1',
            'sec-fetch-dest': 'document',
            'accept-language': 'en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7'
        }

        cookie = Request.get_cookie_by_host(host)
        if cookie:
            headers['cookie'] = cookie
        req.headers.update(headers)
        return req

    def get_authority_by_url(url):
        regex = '(?:http.*://)?(?P<host>[^:/ ]+).?(?P<port>[0-9]*).*'
        match = re.search(regex, url)
        if match:
            return match.group('host')
        return ''

    def get_cookie_by_host(host):
        cases = {
            "www.facebook.com": env.get('COOKIE_WWW_FB_COM'),
            "ads.google.com": env.get('COOKIE_ADS_GOOGLE_COM'),
            "notifications.google.com": env.get('COOKIE_NOTIFICATIONS_GOOGLE_COM')
        }
        return cases.get(host, '')
