import os
import boto3, re, time
import datetime
import json
import urllib.parse
import pandas as pd
import csv
import uuid
import requests
import tempfile

from io import StringIO
from chalice import Chalice, Cron
from datetime import date
from os import environ as env
from chalicelib.s3_client import S3Client
from chalicelib.mailer import Mailer
from chalicelib.request import Request

app = Chalice(app_name='vb-ad-performance-crawler')
if env.get('DEBUG_MODE') == 'TRUE':
    app.debug = True
os.environ['TZ'] = env.get('LOCAL_TIMEZONE', "Asia/Taipei")
time.tzset()

s3_client = S3Client()
s3_bucket = env.get('S3_BUCKET_NAME')
sqs_client = boto3.client('sqs')
exchange_rate = 0

# 每週爬匯率, 然後輸出到 S3 file
# At 00:05 on Monday.
@app.schedule(Cron(0, 0, '?', '*', 'MON', '*'))
def cron_every_monday(event):
    fetch_exchange_rate_to_s3()

# 當原始信件上傳時, 去載報表然後拆分檔案
@app.on_s3_event(events=['s3:ObjectCreated:*'],
                 bucket=s3_bucket,
                 prefix='ses/raws')
def on_mail_uploaded(event):
    ev = event.to_dict()
    app.log.info(ev)
    response = handle_mail_payload(ev)
    app.log.info(response)

# 當拆分後的檔案上傳時, 將資料推進 SQS
@app.on_s3_event(events=['s3:ObjectCreated:*'],
                 bucket=s3_bucket,
                 prefix='ses/reports/chunk_files')
def on_chunk_reports_uploaded(event):
    ev = event.to_dict()
    app.log.info(ev)
    response = push_sensors_event_to_sqs(ev)
    app.log.info("Success: {}, Fail:{}".format(response['success'], response['fail']))

def fetch_exchange_rate_to_s3():
    url = 'https://tw.rter.info/capi.php'
    req=requests.get(url)
    currency = req.json().get('USDTWD', '')
    if currency:
        local_path = ''
        with tempfile.NamedTemporaryFile(mode='w', dir='/tmp', delete=False) as tmp:
            currency = str(currency.get('Exrate', 0))[0:5]
            tmp.write(currency)
            local_path = tmp.name

        if local_path:
            key = env.get('S3_EXCHANGE_RATE_KEY_PATH')
            s3_client.upload(local_path, s3_bucket, key)
            os.remove(local_path)

def handle_mail_payload(event):
    response = {}
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'])
    body = s3_client.read(bucket, key)
    mailer = Mailer(body)
    mail_content = mailer.get_content()
    reference = mailer.get_reference()
    app.log.info("Report Type: {}".format(reference))

    if reference != '' and reference != 'rtb':
        download_link = mailer.get_real_download_link()

        if not download_link:
            app.log.debug("link extract failed. ref: {}, url: {}".format(reference, download_link))
            return ''

        local_path = download_file(download_link, reference)
        if not local_path:
            app.log.debug("report doanload failed. ref: {}, url: {}".format(reference, download_link))
            return ''
        key = 'ses/reports/' + local_path[5:]

        s3_client.upload(local_path, s3_bucket, key)
        chunks_info = chunk_files(local_path, reference)
        for file in chunks_info['files']:
            key = 'ses/reports/chunk_files/' + file[5:]
            if s3_client.upload(file, s3_bucket, key):
                print('Chunk uploaded')
            else:
                app.log.debug('Chunk upload failed. file: {}', file)

            os.remove(file)
        os.remove(local_path)

        response = {
            "to": mail_content['To'],
            "from": mail_content['From'],
            "references": mail_content['References'],
            "reference": reference,
            "chunk_size": chunks_info['chunk_num']
        }
        app.log.info(response)
    return response

def download_file(url, reference):
    if not url:
        return ''

    req = Request.get_request(url)
    local_path = get_local_path(reference)
    with req.get(url, stream=True) as r:
        if r.raise_for_status() is not None:
            return ''

        with open(local_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:
                    f.write(chunk)
    return local_path

def chunk_files(local_path, reference, chunk_line_size=3000):
    chunk_num = 0
    files = []
    header = reference == 'google' and 2 or 0
    with pd.read_csv(local_path, chunksize=chunk_line_size, dtype=str, header=header) as reader:
        for chunk in reader:
            file_name = "{}_part_{}.csv".format(local_path[:-4], chunk_num + 1)
            chunk.to_csv(file_name, index=False)

            files.append(file_name)
            chunk_num += 1
    return {
        'chunk_num': chunk_num,
        'files': files
    }

def push_sensors_event_to_sqs(event):
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'])
    basename = os.path.basename(key)
    body = s3_client.read(bucket, key).decode('utf-8')
    # app.log.debug(body)

    success_count = 0
    fail_count = 0
    total = 0
    reference = basename.split('_')[0]
    si = StringIO(body)
    reader = csv.reader(si, delimiter=',')
    queue_url = sqs_client.get_queue_url(
        QueueName=env.get('SQS_SENSORS_WORKER_QUEUE_NAME'))['QueueUrl']
    for row in reader:
        total += 1
        # app.log.debug(row)
        row = nomalize_to_dict(row, reference)

        if row is not None:
            msg_body = build_message(row, reference)
            response = sqs_client.send_message(QueueUrl=queue_url,
                                        MessageBody=msg_body)
            if response.get('Failed') is None:
                success_count += 1
            else:
                app.log.error("SQS push failed, key: {} at: {} ,".format(key, total))
            success_count += 1
        else:
            fail_count += 1

    return {
        'success': success_count,
        'fail': fail_count,
        'total': total
    }

def nomalize_to_dict(row, reference):
    cases = {
        "facebook": format_facebook_content,
        "google": format_google_content,
        "rtb": format_rtb_content
    }

    func = cases.get(reference, lambda x: [])
    return func(row)

def build_message(row, reference):
    created_at = time.mktime(datetime.datetime.strptime(row['date'], '%Y-%m-%d').timetuple())
    cost = float(row['cost']) * get_exchange_rate_by_reference(reference)
    return json.dumps({
        "common_properties": {
            "created_at": int(created_at) + 86399,
            "platform": env.get('sensors_platform', '0'),
            "site": env.get('sensors_site', 'tw'),
            "is_login": False,
            "client_ip": "127.0.0.1"
        },
        "owner_id": env.get('sensors_owner_id', '2'),
        "event_type": env.get('sensors_event_type', 11),
        "properties": {
            "tid": row['tid'],
            "title": row['title'],
            "cost": cost,
            "utm_source": row['utms'].get('utm_source', ''),
            "utm_medium": row['utms'].get('utm_medium', ''),
            "utm_campaign": row['utms'].get('utm_campaign', ''),
            "utm_term": row['utms'].get('utm_term', ''),
            "utm_content": row['utms'].get('utm_content', '')
        }
    })

def get_exchange_rate_by_reference(reference):
    global exchange_rate
    if exchange_rate == 0:
        key = env.get('S3_EXCHANGE_RATE_KEY_PATH')
        body = s3_client.read(s3_bucket, key)
        if body:
            exchange_rate = float(body)

    cases = {
        "facebook": exchange_rate if exchange_rate else float(env.get('EXCHANGE_RATE_USD_TO_NTD')),
        "google": 1,
        "rtb": 1
    }

    return cases.get(reference)

def format_facebook_content(row):
    # 2021-09-08,"t:616109349820, 2件59 9元 2021夏裝新款純棉短袖T恤女寬鬆T恤短袖學生菸灰上衣",6246808832072,0.30802861685215,utm_source=fanpage&utm_medium=DPA&utm_campaign=basic_plain_buy
    if len(row) != 5 or row[4] == '':
        return None

    reg = '(?P<tid>(t|ali)\:.*)\,(?P<title>.*)'
    match = re.search(reg, row[1])
    if match:
        utms = dict(map(lambda x: (x.split('=')[0], x.split('=')[1]), row[4].split('&')))
        return {
            'date': row[0],
            'tid': match.group('tid'),
            'title': match.group('title').strip(),
            'ad_id': '',
            'cost': "{:.2f}".format(float(row[3])),
            'utms': utms
        }
    return None

def format_google_content(row):
    # 2021-09-08,TW_GSA_SSC(主推商品類別)/Desktop&Mobile,t:643482288122,【夏季新款】夏季薄款寬髮帶遮白髮頭飾時尚包頭巾百搭外出網紅2021年新款頭箍,utm_source=google&utm_medium=SSC&utm_campaign=SSC,TWD,0.00
    if len(row) != 7:
        return None

    if row[0] == '日期':
        return None

    utms = dict(map(lambda x: (x.split('=')[0], x.split('=')[1]), row[4].split('&')))
    return {
        'date': row[0],
        'tid': row[2],
        'title': row[3].strip(),
        'ad_id': '',
        'cost': "{:.2f}".format(float(row[6])),
        'utms': utms
    }

def format_rtb_content(row):
    return None

def read_file(local_path):
    with open(local_path, 'r') as handle:
        for content in enumerate(handle):
            yield content

def get_local_path(ref):
    today = date.today().strftime("%Y_%m_%d")
    return '/tmp/{}_{}_{}.{}'.format(ref, today, uuid.uuid4(), 'csv')
